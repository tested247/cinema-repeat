#include "Cinema.h"

Cinema::Cinema()
{
	numberOfScreens = 0;
	nameOfCinema = "";
	screens = NULL;
}

Cinema::Cinema(string param1 , Screen* param2)
{
	nameOfCinema = param1;
	screens = param2;
}

string Cinema::getName() const
{
	return nameOfCinema;
}
