#ifndef CINEMA
#define CINEMA
#include "Screen.h"
#include <string>
using namespace std;

class Cinema
{
private:
	int numberOfScreens;
	Screen * screens;
	string cinemaName;

public:
	Cinema();
	Cinema(string , Screen*);
	string getName() const;
};

#endif
