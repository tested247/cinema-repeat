#include "Seat.h"

Seat::Seat()
{
	occupied = false;
	isVIP = false;
	seatNum = 0;
}

Seat::Seat(int param , bool param2)
{
	seatNumber = param;
	isVIP = param2;
	occupied = false;
}

bool Seat::isOccupied() const
{
	return occupied;
}

void Seat::seatStatusChange()
{
	occupied = !occupied;
}

int Seat::getSeatNumber() const
{
	return seatNumber;
}

void Seat::setSeatNumber(int param)
{
	seatNumber = param;
}

void Seat::setSeatToEmpty()
{
	occupied = false;
}
