#include <iostream>
#include "Cinema.h"
#include "Screen.h"
#include "Seat.h"
using namespace std;


void main()
{
	Screen* screens = new Screen[6];

	screens[0] = *(new Screen("Screen 1" , 220 , 10 , 10 , 10)); //250 total //220 0rdinary seats. 10 VIP seats. 10 wheelchair seats. 10 VIP wheelchair seats.
	cout << screens[0].getTotalSeats() << endl;

	screens[1] = *(new Screen("Screen 2" , 130 , 10 , 7 , 3)); //150 total //130 ordinary seats. 10 VIP seats. 7 wheelchair seats. 3 VIP wheelchair seats. 
	cout << screens[1].getTotalSeats() << endl;

	screens[2] = *(new Screen("Screen 3" , 120 , 5 , 10 , 15)); //150 total //120 ordinary seats. 5 VIP seats. 10 wheelchair seats. 15 VIP wheelchair commands.
	cout << screens[2].getTotalSeats() << endl;

	screens[3] = *(new Screen("Screen 4" , 90 , 0 , 0 , 10)); //100 total //90 ordinary seats. 0 VIP seats. 0 wheelchair seats. 10 VIP wheelchair seats.
	cout << screens[3].getTotalSeats() << endl;

	screens[4] = *(new Screen("Screen 5" , 40 , 5 , 5 , 0)); //50 total //40 ordinary. 5 VIP seats. 5 wheelchair seats. 0 VIP wheelchair seats.
	cout << screens[4].getTotalSeats() << endl;

	screens[5] = *(new Screen("Screen 6" , 30 , 20 , 10 , 0)); //60 total // 30 ordinary. 20 VIP seats. 10 wheelchair seats. 0 VIP wheelchair seats.
	cout << screens[5].getTotalSeats() << endl;

	Seat* seats1 = new Seat[250];
	
	for (int i = 0; i < 220; i++)
	{
		seats1[i] = *(new Seat(i+1 , false , false));
	}

	for (int i = 220; i < 230; i++)
	{
		seats1[i] = *(new Seat(i+1 , true , false));
	}
	
	for (int i = 230; i < 240; i++)
	{
		seats1[i] = *(new Seat(i+1 , false , true));
	}
	
	for (int i = 240; i < 250; i++)
	{
		seats1[i] = *(new Seat(i+1 , true , true));
	}
	
	screens[0].setSeats(seats1);
	
	Seat* seats2 = new Seat[150];
	
	for (int i = 0; i < 130; i++)
	{
		seats2[i] = *(new Seat(i+1 , false , false));
	}
	//VIP seats in screen.
	for (int i = 130; i < 140; i++)
	{
		seats2[i] = *(new Seat(i+1 , true , false));
	}	
	
	for (int i = 140; i < 147; i++)
	{
		seats2[i] = *(new Seat(i+1 , false , true));
	}	
	
	for (int i = 147; i < 150; i++)
	{
		seats2[i] = *(new Seat(i+1 , true , true));
	}	
	
	screens[1].setSeats(seats2);
	
	Seat* seats3 = new Seat[150];
	
	for (int i = 0; i < 120; i++)
	{
		seats3[i] = *(new Seat(i+1 , false , false));
	}
	
	for (int i = 120; i < 125; i++)
	{
		seats3[i] = *(new Seat(i+1 , true , false));
	}
	
	for (int i = 125; i < 135; i++)
	{
		seats3[i] = *(new Seat(i+1 , false , true));
	}
	
	for (int i = 135; i < 150; i++)
	{
		seats3[i] = *(new Seat(i+1 , true , true));
	}
	
	screens[2].setSeats(seats3);
	
	Seat* seats4 = new Seat[100];
	
	for (int i = 0; i < 90; i++)
	{
		seats4[i] = *(new Seat(i+1 , false ,false));
	}
	
	for (int i = 90; i < 100; i++)
	{
		seats4[i] = *(new Seat(i+1 , true , true));
	}
	
	screens[3].setSeats(seats4);
	
	Seat* seats5 = new Seat[50];
	
	for (int i = 0; i < 40; i++)
	{
		seats5[i] = *(new Seat(i+1 , false , false));
	}
	
	for (int i = 40; i < 45; i++)
	{
		seats5[i] = *(new Seat(i+1 , true , false));
	}
	
	for (int i = 45; i < 50; i++)
	{
		seats5[i] = *(new Seat(i+1 , false , true));
	}
	
	screens[4].setSeats(seats5);
	
	Seat* seats6 = new Seat[60];
	
	for (int i = 0; i < 30; i++)
	{
		seats6[i] = *(new Seat(i+1 , false , false));
	}
	
	for (int i = 30; i < 50; i++)
	{
		seats6[i] = *(new Seat(i+1 , true , false));
	}
	
	for (int i = 50; i < 60; i++)
	{
		seats6[i] = *(new Seat(i+1 , true , false));
	}
	
	screens[5].setSeats(seats6);
	
	Cinema cinema("Athlone IMC" , screens);
	
	int selectedScreenNumber = 0;

	cout << cinema.getName();

	bool keepGoing = true;

	while (keepGoing)
	{
		cout << "--------------------------------------------------------------------" << endl;
		
		cout << "Please enter which screen you are going to: (1-6)" << endl;
		cin >> selectedScreenNumber;

		selectedScreenNumber -= 1;
		if (selectedScreenNumber < 0 || selectedScreenNumber > 5)
		{
			cout << "Invalid number!" << endl;
			break;
		}

		
		screens[selectedScreenNumber].getAllSeats();
		cout << "-------------------------------------------------------------------------------" << endl;

		
		int selectedSeatType = 0;

		cout << "Please enter what type of seat you would like: " << endl;
		cout << "Enter: " << endl;
		cout << "	1 for ordinary \n	2 for VIP \n	3 for wheelchair accessible" << endl;
		cout << "	4 for VIP and wheelchair accessible: " << endl;
		cin >> selectedSeatType;
		cout << "-------------------------------------------------------------------------------" << endl;

	
		if (selectedSeatType == 1)
		{
			cout << "There are " << screens[selectedScreenNumber].getTotalOrdinarySeats() << " ordinary seats in total." << endl;
			int ordinarySeatsOccupied = 0;
			
			Seat * tempSeatArrayPointer = NULL;
		
			if (selectedScreenNumber == 0)
			{
				tempSeatArrayPointer = seats1;
			}
			if (selectedScreenNumber == 1)
			{
				tempSeatArrayPointer = seats2;
			}
			if (selectedScreenNumber == 2)
			{
				tempSeatArrayPointer = seats3;
			}
			if (selectedScreenNumber == 3)
			{
				tempSeatArrayPointer = seats4;
			}
			if (selectedScreenNumber == 4)
			{
				tempSeatArrayPointer = seats5;
			}
			if (selectedScreenNumber == 5)
			{
				tempSeatArrayPointer = seats6;
			}

			
			for (int i = 0; i < screens[selectedScreenNumber].getTotalOrdinarySeats(); i++)
			{
				if (tempSeatArrayPointer[i].isOccupied() == true)
				{
					ordinarySeatsOccupied++;
				}
			
			}
			int seatsWanted = 0;
			cout << ordinarySeatsOccupied << " of these seats are occupied." << endl;
			cout << "How many seats would you like to occupy: " << endl;
			cin >> seatsWanted;
			
			
			for (int i = 0; i < screens[selectedScreenNumber].getTotalOrdinarySeats(); i++)
			{
				
				if (tempSeatArrayPointer[i].isOccupied() == false)
				{	
					tempSeatArrayPointer[i].seatStatusChange();
					cout << tempSeatArrayPointer[i].getSeatNumber() << " has been booked." << endl;
					seatsWanted--;
				}
		
				if (seatsWanted <= 0)
				{
					cout << "-------------------------------------------------------------------------------" << endl;
					break;
				}
			}
		}

		//VIP seats.
		if (selectedSeatType == 2)
		{
			cout << "There are " << screens[selectedScreenNumber].getTotalVIPSeats() << " VIP seats in total." << endl;
			int VIPSeatsOccupied = 0;
			//Getting seat array to be used.
			Seat * tempSeatArrayPointer = NULL;
			//Setting the pointer to the correct seat array.
			if (selectedScreenNumber == 0)
			{
				tempSeatArrayPointer = seats1;
			}

			if (selectedScreenNumber == 1)
			{
				tempSeatArrayPointer = seats2;
			}

			if (selectedScreenNumber == 2)
			{
				tempSeatArrayPointer = seats3;
			}

			if (selectedScreenNumber == 3)
			{
				tempSeatArrayPointer = seats4;
			}

			if (selectedScreenNumber == 4)
			{
				tempSeatArrayPointer = seats5;
			}

			if (selectedScreenNumber == 5)
			{
				tempSeatArrayPointer = seats6;
			}

			//Iterating through the seat array.
			for (int i = screens[selectedScreenNumber].getTotalOrdinarySeats(); i < (screens[selectedScreenNumber].getTotalVIPSeats() + screens[selectedScreenNumber].getTotalOrdinarySeats()); i++)
			{
				if (tempSeatArrayPointer[i].isOccupied() == true)
				{
					VIPSeatsOccupied++;
				}
			
			}
			int seatsWanted = 0;
			cout << VIPSeatsOccupied << " of these seats are occupied." << endl;
			cout << "How many seats would you like to occupy: " << endl;
			cin >> seatsWanted;
			
			//Iterate through array and return seat numbers available.
			for (int i = screens[selectedScreenNumber].getTotalOrdinarySeats(); i < (screens[selectedScreenNumber].getTotalOrdinarySeats() + screens[selectedScreenNumber].getTotalVIPSeats()); i++)
			{
				//Checks if seat is unoccupied, if so, the seat is booked.
				if (tempSeatArrayPointer[i].isOccupied() == false)
				{	
					tempSeatArrayPointer[i].seatStatusChange();
					cout << tempSeatArrayPointer[i].getSeatNumber() << " has been booked." << endl;
					seatsWanted--;	
				}
				//If seats wanted is equal to 0, don't book any more seats.
				if (seatsWanted <= 0)
				{
					cout << "-------------------------------------------------------------------------------" << endl;
					break;
				}
			}
		

		}

		//Wheelchair Accessible.
		if (selectedSeatType == 3)
		{
			cout << "There are " << screens[selectedScreenNumber].getTotalWheelchairAccessibleSeats() << " wheelchair accessible seats in total." << endl;
			int wheelchairSeatsOccupied = 0;
			//Getting seat array to be used.
			Seat * tempSeatArrayPointer = NULL;

			if (selectedScreenNumber == 0)
			{
				tempSeatArrayPointer = seats1;
			}

			if (selectedScreenNumber == 1)
			{
				tempSeatArrayPointer = seats2;
			}

			if (selectedScreenNumber == 2)
			{
				tempSeatArrayPointer = seats3;
			}

			if (selectedScreenNumber == 3)
			{
				tempSeatArrayPointer = seats4;
			}

			if (selectedScreenNumber == 4)
			{
				tempSeatArrayPointer = seats5;
			}

			if (selectedScreenNumber == 5)
			{
				tempSeatArrayPointer = seats6;
			}
			//Iterating through the seat array.
			for (int i = (screens[selectedScreenNumber].getTotalVIPSeats() + screens[selectedScreenNumber].getTotalOrdinarySeats()); i < (screens[selectedScreenNumber].getTotalVIPSeats() + screens[selectedScreenNumber].getTotalOrdinarySeats() + screens[selectedScreenNumber].getTotalWheelchairAccessibleSeats()); i++)
			{
				if (tempSeatArrayPointer[i].isOccupied() == true)
				{
					wheelchairSeatsOccupied++;
				}
			}

			int seatsWanted = 0;
			cout << wheelchairSeatsOccupied << " of these seats are occupied." << endl;
			cout << "How many seats would you like to occupy: " << endl;
			cin >> seatsWanted;
			
			//Iterate through array and return seat numbers available.
			for (int i = (screens[selectedScreenNumber].getTotalVIPSeats() + screens[selectedScreenNumber].getTotalOrdinarySeats()); i < (screens[selectedScreenNumber].getTotalVIPSeats() + screens[selectedScreenNumber].getTotalOrdinarySeats() + screens[selectedScreenNumber].getTotalWheelchairAccessibleSeats()); i++)
			{
				//Checks if seat is unoccupied, if so, the seat is booked.
				if (tempSeatArrayPointer[i].isOccupied() == false)
				{	
					tempSeatArrayPointer[i].seatStatusChange();
					cout << tempSeatArrayPointer[i].getSeatNumber() << " has been booked." << endl;
					seatsWanted--;	
				}
				//If seats wanted is equal to 0, don't book any more seats.
				if (seatsWanted <= 0)
				{
					cout << "-------------------------------------------------------------------------------" << endl;
					break;
				}
			}
		}

		//VIP and wheelchair accessible.
		if (selectedSeatType == 4)
		{
			cout << "There are " << screens[selectedScreenNumber].getTotalVIPWheelchairAccessibleSeats() << " VIP and wheelchair accessible seats in total." << endl;
			int VIPWheelchairSeatsOccupied = 0;
			//Getting seat array to be used.
			Seat * tempSeatArrayPointer = NULL;

			if (selectedScreenNumber == 0)
			{
				tempSeatArrayPointer = seats1;
			}

			if (selectedScreenNumber == 1)
			{
				tempSeatArrayPointer = seats2;
			}

			if (selectedScreenNumber == 2)
			{
				tempSeatArrayPointer = seats3;
			}

			if (selectedScreenNumber == 3)
			{
				tempSeatArrayPointer = seats4;
			}

			if (selectedScreenNumber == 4)
			{
				tempSeatArrayPointer = seats5;
			}

			if (selectedScreenNumber == 5)
			{
				tempSeatArrayPointer = seats6;
			}
			//Iterating through the seat array.
			for (int i = (screens[selectedScreenNumber].getTotalOrdinarySeats() + screens[selectedScreenNumber].getTotalVIPSeats() + screens[selectedScreenNumber].getTotalWheelchairAccessibleSeats()) ; i < (screens[selectedScreenNumber].getTotalOrdinarySeats() + screens[selectedScreenNumber].getTotalVIPSeats() + screens[selectedScreenNumber].getTotalWheelchairAccessibleSeats() + screens[selectedScreenNumber].getTotalVIPWheelchairAccessibleSeats()); i++)
			{
				if (tempSeatArrayPointer[i].isOccupied() == true)
				{
					VIPWheelchairSeatsOccupied++;
				}
			
			}

			int seatsWanted = 0;
			cout << VIPWheelchairSeatsOccupied << " of these seats are occupied." << endl;
			cout << "How many seats would you like to occupy: " << endl;
			cin >> seatsWanted;
			
			//Iterate through array and return seat numbers available.
			for (int i = (screens[selectedScreenNumber].getTotalOrdinarySeats() + screens[selectedScreenNumber].getTotalVIPSeats() + screens[selectedScreenNumber].getTotalWheelchairAccessibleSeats()) ; i < (screens[selectedScreenNumber].getTotalOrdinarySeats() + screens[selectedScreenNumber].getTotalVIPSeats() + screens[selectedScreenNumber].getTotalWheelchairAccessibleSeats() + screens[selectedScreenNumber].getTotalVIPWheelchairAccessibleSeats()); i++)
			{
				//Checks if seat is unoccupied, if so, the seat is booked.
				if (tempSeatArrayPointer[i].isOccupied() == false)
				{
					
					tempSeatArrayPointer[i].seatStatusChange();
					cout << tempSeatArrayPointer[i].getSeatNumber() << " has been booked." << endl;
					seatsWanted--;
				}
				//If seats wanted is equal to 0, don't book any more seats.
				if (seatsWanted <= 0)
				{
					cout << "-------------------------------------------------------------------------------" << endl;
					break;
				}
			}
		
		}
		//Ask user if they wish to continue.
		char continueDecision = NULL;
		cout << "Do you wish to continue? " << endl;
		cout << "Type y for yes and n for no: " << endl;
		cin >> continueDecision;
		if (continueDecision == 'y' || continueDecision == 'Y')
		{
			keepGoing = true;
			//Ask user if they wish to reset the seats of a selected screen to empty.
			cout << "-------------------------------------------------------------------------------" << endl;
			cout << "Do you wish to clear the seats of a certain screen?" << endl;
			char resetDecision = NULL;
			cout << "	Type y for yes and n for no: " << endl;
			cin >> resetDecision;
			if (resetDecision == 'y' || resetDecision == 'Y')
			{
				int screenToReset = 0;
				cout << "-------------------------------------------------------------------------------" << endl;
				cout << "-------------------------------------------------------------------------------" << endl;
				cout << "	What screen would you like to reset? (1-6)" << endl;
				cin >> screenToReset;

				screenToReset -= 1;
				//If a proper number isn't entered.
				if (screenToReset < 0 || screenToReset > 5)
				{
					cout << "Invalid number!" << endl;
					break;
				}

				Seat* seatArrayToResetPointer = NULL;
				//Setting pointer to the right array.
				if (screenToReset == 0)
				{
					seatArrayToResetPointer = seats1;
				}
				if (screenToReset == 1)
				{
					seatArrayToResetPointer = seats2;
				}
				if (screenToReset == 2)
				{
					seatArrayToResetPointer = seats3;
				}
				if (screenToReset == 3)
				{
					seatArrayToResetPointer = seats4;
				}
				if (screenToReset == 4)
				{
					seatArrayToResetPointer = seats5;
				}
				if (screenToReset == 5)
				{
					seatArrayToResetPointer = seats6;
				}
				//Reseting the seats in the selected screen.
				for (int i = 0; i < screens[screenToReset].getTotalSeats(); i++)
				{
					seatArrayToResetPointer[i].setSeatToEmpty();
				}
			}

			continue;
		}
		else
		{
			keepGoing = false;
		}
	}//END OF WHILE LOOP
	cout << "-------------------------------------------------------------------------------" << endl;
	cout << "-------------------------------------------------------------------------------" << endl;
	cout << "-------------------------------GOOD BYE----------------------------------------" << endl;
	cout << "-------------------------------------------------------------------------------" << endl;
	cout << "-------------------------------------------------------------------------------" << endl;
	system("pause");
}


