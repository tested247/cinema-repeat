#ifndef SEAT
#define SEAT

class Seat
{
private:
	bool occupied;
	bool isVIP;
	int seatNum;

public:
	Seat();
	Seat(int , bool);
	bool isOccupied() const;
	void seatStatusChange();
	int getSeatNum() const;
	void setSeatNum(int);
	void Seat::setSeatToEmpty();
};

#endif
