#include "Cinema.h"

Cinema::Cinema()
{
	screensAvailable = 0;
	cinemaName = "";
	screens = NULL;
}

Cinema::Cinema(string aCinemaName, Screen* aScreenName)
{
	cinemaName = aCinemaName;
	screens = aScreenName;
}

string Cinema::getName() const
{
	return cinemaName;
}
