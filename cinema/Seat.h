#ifndef SEAT
#define SEAT

class Seat
{
private:
	bool taken;
	bool isVIP;
	int seatNum;

public:
	Seat();
	Seat(int, bool);
	bool isTaken() const;
	void seatStatusChange();
	int getSeatNum() const;
	void setSeatNum(int);
	void Seat::setSeatToEmpty();
};

#endif
