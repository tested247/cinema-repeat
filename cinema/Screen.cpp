#include "Screen.h"

Screen::Screen()
{
	screenName = "";
	totalSeats = 0;
	standardSeats = 0;
	VIPSeats = 0;
	Seat *seats;
}

Screen::Screen(string aScreen, int aSeat, int aStandardSeat, int aVIPSeat)
{
	screenName = aScreen;
	totalSeats = aSeat;
	standardSeats = aStandardSeat;
	VIPSeats = aVIPSeat;
}

string Screen::getScreenName() const
{
	return screenName;
}

void Screen::setScreenName(string aScreen)
{
	screenName = aScreen;
}

int Screen::getTotalSeats()
{
	return totalSeats;
}

int Screen::getTotalStandardSeats() const
{
	return standardSeats;
}

void Screen::setTotalStandardSeats(int aStandardSeat)
{
	standardSeats = aStandardSeat;
}

int Screen::getTotalVIPSeats() const
{
	return VIPSeats;
}

void Screen::setTotalVIPSeats(int aVIPSeat)
{
	VIPSeats = aVIPSeat;
}

void Screen::getAllSeats() const
{
	
}

void Screen::setSeats(Seat*)
{

}

Seat Screen::getSeatArray()
{

}