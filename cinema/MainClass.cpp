#include <iostream>
#include "Cinema.h"
#include "Screen.h"
#include "Seat.h"
using namespace std;


void main()
{
	Screen* screens = new Screen[6];

	screens[0] = *(new Screen("Screen 1", 220, 10)); 
	cout << screens[0].getTotalSeats() << endl;

	screens[1] = *(new Screen("Screen 2", 130, 10));  
	cout << screens[1].getTotalSeats() << endl;

	screens[2] = *(new Screen("Screen 3", 120, 5)); 
	cout << screens[2].getTotalSeats() << endl;

	screens[3] = *(new Screen("Screen 4", 90, 0)); 
	cout << screens[3].getTotalSeats() << endl;

	screens[4] = *(new Screen("Screen 5", 40, 5));
	cout << screens[4].getTotalSeats() << endl;

	screens[5] = *(new Screen("Screen 6", 30, 20)); 
	cout << screens[5].getTotalSeats() << endl;

	Seat* seats1 = new Seat[250];

	for (int i = 0; i < 220; i++)
	{
		seats1[i] = *(new Seat(i + 1, false, false));
	}

	for (int i = 220; i < 230; i++)
	{
		seats1[i] = *(new Seat(i + 1, true, false));
	}

	for (int i = 230; i < 240; i++)
	{
		seats1[i] = *(new Seat(i + 1, false, true));
	}

	for (int i = 240; i < 250; i++)
	{
		seats1[i] = *(new Seat(i + 1, true, true));
	}

	screens[0].setSeats(seats1);

	Seat* seats2 = new Seat[150];

	for (int i = 0; i < 130; i++)
	{
		seats2[i] = *(new Seat(i + 1, false, false));
	}
	
	for (int i = 130; i < 140; i++)
	{
		seats2[i] = *(new Seat(i + 1, true, false));
	}

	for (int i = 140; i < 147; i++)
	{
		seats2[i] = *(new Seat(i + 1, false, true));
	}

	for (int i = 147; i < 150; i++)
	{
		seats2[i] = *(new Seat(i + 1, true, true));
	}

	screens[1].setSeats(seats2);

	Seat* seats3 = new Seat[150];

	for (int i = 0; i < 120; i++)
	{
		seats3[i] = *(new Seat(i + 1, false, false));
	}

	for (int i = 120; i < 125; i++)
	{
		seats3[i] = *(new Seat(i + 1, true, false));
	}

	for (int i = 125; i < 135; i++)
	{
		seats3[i] = *(new Seat(i + 1, false, true));
	}

	for (int i = 135; i < 150; i++)
	{
		seats3[i] = *(new Seat(i + 1, true, true));
	}

	screens[2].setSeats(seats3);

	Seat* seats4 = new Seat[100];

	for (int i = 0; i < 90; i++)
	{
		seats4[i] = *(new Seat(i + 1, false, false));
	}

	for (int i = 90; i < 100; i++)
	{
		seats4[i] = *(new Seat(i + 1, true, true));
	}

	screens[3].setSeats(seats4);

	Seat* seats5 = new Seat[50];

	for (int i = 0; i < 40; i++)
	{
		seats5[i] = *(new Seat(i + 1, false, false));
	}

	for (int i = 40; i < 45; i++)
	{
		seats5[i] = *(new Seat(i + 1, true, false));
	}

	for (int i = 45; i < 50; i++)
	{
		seats5[i] = *(new Seat(i + 1, false, true));
	}

	screens[4].setSeats(seats5);

	Seat* seats6 = new Seat[60];

	for (int i = 0; i < 30; i++)
	{
		seats6[i] = *(new Seat(i + 1, false, false));
	}

	for (int i = 30; i < 50; i++)
	{
		seats6[i] = *(new Seat(i + 1, true, false));
	}

	for (int i = 50; i < 60; i++)
	{
		seats6[i] = *(new Seat(i + 1, true, false));
	}

	screens[5].setSeats(seats6);

	Cinema cinema("Athlone IMC", screens);

	int selectedScreenNum = 0;

	cout << cinema.getName();

	bool keepGoing = true;

	while (keepGoing)
	{
		cout << " " << endl;
		cout << "Please enter which screen you are going to: (1-6)" << endl;
		cin >> selectedScreenNum;

		selectedScreenNum -= 1;
		if (selectedScreenNum < 0 || selectedScreenNum > 5)
		{
			cout << "Invalid Num!" << endl;
			break;
		}


		screens[selectedScreenNum].getAllSeats();
		cout << " " << endl;


		int selectedSeatType = 0;

		cout << "Please enter what type of seat you would like: " << endl;
		cout << "Enter: " << endl;
		cout << "	1 for ordinary \n	2 for VIP "<< endl;
		cin >> selectedSeatType;
		cout << " " << endl;


		if (selectedSeatType == 1)
		{
			cout << "There are " << screens[selectedScreenNum].getTotalStandardSeats() << " Standard seats in total." << endl;
			int StandardSeatsTaken = 0;

			Seat * tempSeatArrayPointer = NULL;

			if (selectedScreenNum == 0)
			{
				tempSeatArrayPointer = seats1;
			}
			if (selectedScreenNum == 1)
			{
				tempSeatArrayPointer = seats2;
			}
			if (selectedScreenNum == 2)
			{
				tempSeatArrayPointer = seats3;
			}
			if (selectedScreenNum == 3)
			{
				tempSeatArrayPointer = seats4;
			}
			if (selectedScreenNum == 4)
			{
				tempSeatArrayPointer = seats5;
			}
			if (selectedScreenNum == 5)
			{
				tempSeatArrayPointer = seats6;
			}


			for (int i = 0; i < screens[selectedScreenNum].getTotalStandardSeats(); i++)
			{
				if (tempSeatArrayPointer[i].isTaken() == true)
				{
					StandardSeatsTaken++;
				}

			}
			int seatsWanted = 0;
			cout << StandardSeatsTaken << " of these seats are Taken." << endl;
			cout << "How many seats would you like to occupy: " << endl;
			cin >> seatsWanted;


			for (int i = 0; i < screens[selectedScreenNum].getTotalStandardSeats(); i++)
			{

				if (tempSeatArrayPointer[i].isTaken() == false)
				{
					tempSeatArrayPointer[i].seatStatusChange();
					cout << tempSeatArrayPointer[i].getSeatNum() << "You seats have been booked." << endl;
					seatsWanted--;
				}

				if (seatsWanted <= 0)
				{
					cout << " " << endl;
					break;
				}
			}
		}

		if (selectedSeatType == 2)
		{
			cout << "There are " << screens[selectedScreenNum].getTotalVIPSeats() << " VIP seats in total." << endl;
			int VIPSeatsTaken = 0;
			Seat * tempSeatArrayPointer = NULL;
			if (selectedScreenNum == 0)
			{
				tempSeatArrayPointer = seats1;
			}

			if (selectedScreenNum == 1)
			{
				tempSeatArrayPointer = seats2;
			}

			if (selectedScreenNum == 2)
			{
				tempSeatArrayPointer = seats3;
			}

			if (selectedScreenNum == 3)
			{
				tempSeatArrayPointer = seats4;
			}

			if (selectedScreenNum == 4)
			{
				tempSeatArrayPointer = seats5;
			}

			if (selectedScreenNum == 5)
			{
				tempSeatArrayPointer = seats6;
			}

			//Iterating through the seat array.
			for (int i = screens[selectedScreenNum].getTotalStandardSeats(); i < (screens[selectedScreenNum].getTotalVIPSeats() + screens[selectedScreenNum].getTotalStandardSeats()); i++)
			{
				if (tempSeatArrayPointer[i].isTaken() == true)
				{
					VIPSeatsTaken++;
				}

			}
			int seatsWanted = 0;
			cout << VIPSeatsTaken << " of these seats are Taken." << endl;
			cout << "How many seats would you like to occupy: " << endl;
			cin >> seatsWanted;

			
			for (int i = screens[selectedScreenNum].getTotalStandardSeats(); i < (screens[selectedScreenNum].getTotalStandardSeats() + screens[selectedScreenNum].getTotalVIPSeats()); i++)
			{
				
				if (tempSeatArrayPointer[i].isTaken() == false)
				{
					tempSeatArrayPointer[i].seatStatusChange();
					cout << tempSeatArrayPointer[i].getSeatNum() << " has been booked." << endl;
					seatsWanted--;
				}
				if (seatsWanted <= 0)
				{
					cout << " " << endl;
					break;
				}
			}
		}

		char carryOn = NULL;
		cout << "Do you wish to continue? " << endl;
		cout << "Type y for yes and n for no: " << endl;
		cin >> carryOn;
		if (carryOn== 'y' || carryOn == 'Y')
		{
			keepGoing = true;
			cout << " " << endl;
			cout << "Do you wish to clear the seats of a certain screen?" << endl;
			char resetOption = NULL;
			cout << "Type y for yes and n for no: " << endl;
			cin >> resetOption;
			if (resetOption == 'y' || resetOption == 'Y')
			{
				int screenToReset = 0;
				cout << " " << endl;
				cout << " " << endl;
				cout << "What screen would you like to reset? (1-6)" << endl;
				cin >> screenToReset;

				screenToReset -= 1;
				if (screenToReset < 0 || screenToReset > 5)
				{
					cout << "Invalid Num!" << endl;
					break;
				}

				Seat* seatArrayToResetPointer = NULL;
				if (screenToReset == 0)
				{
					seatArrayToResetPointer = seats1;
				}
				if (screenToReset == 1)
				{
					seatArrayToResetPointer = seats2;
				}
				if (screenToReset == 2)
				{
					seatArrayToResetPointer = seats3;
				}
				if (screenToReset == 3)
				{
					seatArrayToResetPointer = seats4;
				}
				if (screenToReset == 4)
				{
					seatArrayToResetPointer = seats5;
				}
				if (screenToReset == 5)
				{
					seatArrayToResetPointer = seats6;
				}
				for (int i = 0; i < screens[screenToReset].getTotalSeats(); i++)
				{
					seatArrayToResetPointer[i].setSeatToEmpty();
				}
			}

			continue;
		}
		else
		{
			keepGoing = false;
		}
	}
	
	cout << "GOOD BYE AND THANK YOU FOR USING OUR SERVICES" << endl;
	system("pause");
}



