#include "Seat.h"

Seat::Seat()
{
	taken = false;
	isVIP = false;
	seatNum = 0;
}

Seat::Seat(int aSeatNum, bool aVIP)
{
	seatNum = aSeatNum;
	isVIP = aVIP;
	taken = false;
}

bool Seat::isTaken() const
{
	return taken;
}

void Seat::seatStatusChange()
{
	taken = !taken;
}

int Seat::getSeatNum() const
{
	return seatNum;
}

void Seat::setSeatNum(int aSeatNum)
{
	seatNum = aSeatNum;
}

void Seat::setSeatToEmpty()
{
	taken = false;
}
