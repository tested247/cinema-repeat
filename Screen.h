#ifndef SCREEN
#define SCREEN
#include "Seat.h"
#include <string>
#include <iostream>
using namespace std;

class Screen
{
private:
	string screenName;
	int totalSeats;
	int standardSeats;
	int VIPSeats;
	Seat *seats;

public:
	Screen();
	Screen(string , int , int , int);
	string getScreenName() const;
	void setScreenName(string);
	int getTotalSeats();
	int getTotalStandardSeats() const;
	void setTotalStandardSeats(int);
	int getTotalVIPSeats() const;
	void setTotalVIPSeats(int);
	void getAllSeats() const;
	void setSeats(Seat*);
	Seat getSeatArray();
};

#endif
